﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeSnipper
{
    class MyCalc
    {
        public event KnowAnswer OnKnowAnswer;

        public void Add(int i1, int i2 )
        {
           AnswerEventArgs e = new AnswerEventArgs();
            e.answer = i1 + i2;

            OnKnowAnswer(this, e);
        }
    }
}
