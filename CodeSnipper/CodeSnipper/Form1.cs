﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace CodeSnipper
{
    public delegate void KnowAnswer(object sender, AnswerEventArgs e);
    public partial class Form1 : Form
    {
        MyCalc mc = new MyCalc();
        public Form1()
        {
            mc.OnKnowAnswer += mc_OnKnowAnswer;
            InitializeComponent();
            
        }

        void mc_OnKnowAnswer(object sender, AnswerEventArgs e)
        {
            Console.WriteLine("The answer is:" + e.answer);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Event Trigger
            mc.Add(3, 5);
        }
    }
}
